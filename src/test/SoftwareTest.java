package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.Card;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card stu1 = new Card(600);
		stu1.getname("Balance: ");
		frame.setResult(stu1.toString()+stu1.getBalance());
		stu1.withdraw(300);
		frame.extendResult("Withdraw: "+"300");
		stu1.deposit(200);
		frame.extendResult("Deposit: "+"200");
		frame.extendResult(stu1.toString()+stu1.getBalance());
	}

	ActionListener list;
	SoftwareFrame frame;
}
