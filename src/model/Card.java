package model;

public class Card {
	private int balance;
	private String name;
	private int amount;
	
	public Card(int balance) {
		this.balance = balance;
	}
	public void withdraw(int amount){
		 balance = balance-amount;
	}
	public void deposit(int amount){
		 balance+=amount;
	}
	public void getname(String str) {
		name = str;
	}
	public String toString() {
		return name;
	}
	public int getBalance(){
		return balance;
	}

	

}
